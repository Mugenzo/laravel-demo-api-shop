<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'title' => 'Материнські плати',
                'slug' => 'motherboards'
            ],
            [
                'title' => 'Процессори',
                'slug' => 'processors'
            ],
            [
                'title' => 'Оперативна пам\'ять',
                'slug' => 'ram'
            ],
            [
                'title' => 'Відеокарти',
                'slug' => 'videocards'
            ],
            [
                'title' => 'Блоки живлення',
                'slug' => 'psu'
            ],
            [
                'title' => 'Корпуси',
                'slug' => 'cases'
            ],
        ];

        foreach($data as $category):
            Category::create($category);
        endforeach;
    }
}
