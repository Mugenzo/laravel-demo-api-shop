<?php

namespace Database\Seeders;

use App\Models\Attribute;
use App\Models\AttributeValue;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'title' => 'Бренд',
                'values' => [
                    'AMD',
                    'Intel',
                    'ASRock',
                    'ASUS',
                    'Gigabyte',
                    'NZXT',
                    'Corsair',
                    'Crucial',
                    'Goodram',
                    'Hynix',
                    'Kingston',
                    'Chieftech',
                    'MSI',
                    'GameMax',
                    'Zalman',
                    'Aerocool',
                    'DeepCool',
                    'Lian Li',
                    'Vinga'
                ]
            ],
            [
                'title' => 'Обсяг пам\'яті',
                'values' => [
                    '10 Гб',
                    '12 Гб',
                    '16 Гб',
                    '20 Гб',
                    '24 Гб',
                    '32 Гб',
                    '64 Гб',
                    '128 Гб'
                ]
            ],
            [
                'title' => 'Частота пам\'яті',
                'values' => [
                    '4800 МГц',
                    '5400 МГц',
                    '5400 МГц',
                    '6000 МГц',
                    '6200 МГц',
                ]
            ],
            [
                'title' => 'Тип пам\'яті',
                'values' => [
                    'DDR4 SDRAM',
                    'DDR5 SDRAM',
                ]
            ],
            [
                'title' => 'Сокет',
                'values' => [
                    'Socket 1200',
                    'Socket 1700',
                    'Socket 2011',
                    'Socket AM4',
                    'Socket AM5',
                ]
            ],
            [
                'title' => 'Тип роз\'єму',
                'values' => [
                    'Socket 1200',
                    'Socket 1700',
                    'Socket 2011',
                    'Socket AM4',
                    'Socket AM5',
                ]
            ],
            [
                'title' => 'Сімейство процесора',
                'values' => [
                    'Intel Core i3',
                    'Intel Core i5',
                    'Intel Core i7',
                    'AMD Ryzen 3',
                    'AMD Ryzen 5',
                    'AMD Ryzen 7',
                ]
            ],
            [
                'title' => 'Чипсет',
                'values' => [
                    'AMD 690G',
                    'Intel B460',
                    'Intel Z390',
                    'Intel B660',
                    'Intel B760',
                    'Intel C600',
                    'Intel C602',
                    'AMD B350',
                    'AMD B450',
                    'AMD X570',
                    'AMD B650',
                    'AMD X670',
                ]
            ],
            [
                'title' => 'Формфактор',
                'values' => [
                    'ATX',
                    'EATX',
                    'MicroATX'
                ]
            ],
            [
                'title' => 'Підтримка пам\'яті',
                'values' => [
                    'DDR4',
                    'DDR5'
                ]
            ],
            [
                'title' => 'Виробник графічного процесора',
                'values' => [
                    'AMD',
                    'nVidia'
                ]
            ],
            [
                'title' => 'Інтерфейс',
                'values' => [
                    'PCI-Express 4.0',
                    'PCI-Express 4.0 x8',
                    'PCI-Express 4.0 x16',
                ]
            ],
            [
                'title' => 'Потужність',
                'values' => [
                    '400 - 500 Вт',
                    '501 - 650 Вт',
                    '651 - 800 Вт',
                    '801 - 1000 Вт',
                ]
            ],
            [
                'title' => 'Підтримка 80 PLUS',
                'values' => [
                    '80 PLUS',
                    '80 PLUS Bronze',
                    '80 PLUS Silver',
                    '80 PLUS Gold',
                ]
            ],
            [
                'title' => 'Форм-фактор БЖ',
                'values' => [
                    'ATX',
                    'ATX PS/2',
                    'BTX',
                ]
            ],
            [
                'title' => 'Форм-фактор материнської плати',
                'values' => [
                    'AT',
                    'ATX',
                    'EATX',
                    'Full ATX'
                ]
            ]
        ];


        foreach($data as $attribute):
            $attr = Attribute::create([
                'title' => $attribute['title']
            ]);

            foreach($attribute['values'] as $value):
                AttributeValue::create([
                    'title' => $value,
                    'attribute_id' => $attr->id
                ]);
            endforeach;
        endforeach;
    }
}
